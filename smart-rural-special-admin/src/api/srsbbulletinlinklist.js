import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/bulletin/srsbbulletinlinklist/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/bulletin/srsbbulletinlinklist',
    method: 'post',
    data: obj
  })
}

// 生成外链
export function createLinked(obj) {
  return request({
    url: '/bulletin/srsbbulletinlinklist/create',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/bulletin/srsbbulletinlinklist/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/bulletin/srsbbulletinlinklist/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/bulletin/srsbbulletinlinklist',
    method: 'put',
    data: obj
  })
}
