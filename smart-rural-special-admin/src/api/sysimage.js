

import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/admin/sysimage/page',
    method: 'get',
    params: query
  })
}

export function addObj(file,fileName) {
  return request({
    headers: {"content-type": "multipart/form-data"},
    url: `/admin/sysimage/${fileName}`,
    method: 'post',
    data: file
  })
}

export function getObj(id) {
  return request({
    url: '/admin/sysimage/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/admin/sysimage/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/admin/sysimage',
    method: 'put',
    data: obj
  })
}
