
import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/bulletin/srsbbulletintaxinformation/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/bulletin/srsbbulletintaxinformation',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/bulletin/srsbbulletintaxinformation/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/bulletin/srsbbulletintaxinformation/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/bulletin/srsbbulletintaxinformation',
    method: 'put',
    data: obj
  })
}
