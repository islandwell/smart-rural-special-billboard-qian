
import request from '@/router/axios'

export function fetchList(query) {
  return request({
    url: '/bulletin/srsbbulletinwordcloudinfo/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/bulletin/srsbbulletinwordcloudinfo',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/bulletin/srsbbulletinwordcloudinfo/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/bulletin/srsbbulletinwordcloudinfo/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/bulletin/srsbbulletinwordcloudinfo',
    method: 'put',
    data: obj
  })
}
