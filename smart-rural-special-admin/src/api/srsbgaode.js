import request from '@/router/axios'

export function getGeoLocation(obj) {
    return request({
      url: '/bulletin/authBulletin/getGeoLocation',
      method: 'post',
      data: obj
    })
  }