export const tableOption = {
  "column": [{
    "type": "input",
    "label": "主键id",
    "prop": "id",
    "addDisplay": false,
    "editDisabled": false
  }, {
    "type": "input",
    "label": "农户名称",
    "prop": "name",
    search: true,
    "addDisplay": false,
    "editDisabled": false,
    "display": true
  }, {
    "type": "input",
    "label": "地址",
    "prop": "address",
    "addDisplay": false,
    "editDisabled": false
  }, {
    "type": "number",
    "label": "总种植面积(亩)",
    "prop": "area",
    "addDisplay": false,
    "editDisabled": false,
    "display": true,
    "controls": true
  }, {
    "type": "number",
    "label": "销量(元)",
    "prop": "sales",
    "addDisplay": false,
    "editDisabled": false,
    "controls": true,
    "display": true
  }, {
    "type": "input",
    "label": "创建时间",
    "prop": "createTime",
    "addDisplay": false,
    "editDisabled": false
  }, {
    "type": "input",
    "label": "创建人",
    "prop": "createBy",
    "addDisplay": false,
    "editDisabled": false,
    "display": false,
    "readonly": true
  }, {
    "type": "input",
    "label": "更新时间",
    "prop": "updateTime",
    "addDisplay": false,
    "editDisabled": false,
    "readonly": true
  }, {
    "type": "input",
    "label": "更新人",
    "prop": "updateBy",
    "addDisplay": false,
    "editDisabled": false,
    "readonly": true,
    "showWordLimit": false
  }],
  "labelPosition": "left",
  "labelSuffix": "：",
  "labelWidth": 120,
  "gutter": 0,
  "menuBtn": true,
  "submitBtn": true,
  "submitText": "提交",
  "emptyBtn": true,
  "emptyText": "清空",
  "menuPosition": "center",
  "border": true,
  "index": true,
  "indexLabel": "序号",
  "stripe": true,
  "menuAlign": "center",
  "align": "center",
  "searchMenuSpan": 6
}
