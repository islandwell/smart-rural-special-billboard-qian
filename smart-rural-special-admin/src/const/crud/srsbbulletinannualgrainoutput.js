export const tableOption = {
  column: [{
      type: 'input',
      label: 'id',
      prop: 'id',
      addDisplay: false,
      editDisabled: false
    },
    {
      type: 'year',
      label: '年份',
      prop: 'particularYear',
      search: true,
      addDisplay: false,
      editDisabled: false,
      display: true,
      format: 'yyyy',
      valueFormat: 'yyyy'
    },
    {
      type: 'input',
      label: '全年粮食播种面积',
      prop: 'annualGrainSowingArea',
      addDisplay: false,
      editDisabled: false,
      display: true,
      readonly: false
    },
    {
      type: 'input',
      label: '单产值',
      prop: 'unitYieldValue',
      addDisplay: false,
      display: true,
      editDisabled: false,
      editDisabled: false
    },
    {
      type: 'input',
      label: '总产值',
      prop: 'totalOutputValue',
      addDisplay: false,
      display: true,
      editDisabled: false
    },
    {
      type: 'input',
      label: '夏粮亩产',
      prop: 'summerGrainYieldPerMu',
      addDisplay: false,
      display: true,
      editDisabled: false
    },
    {
      type: 'input',
      label: '夏粮总产',
      prop: 'summerGrainTotalOutput',
      addDisplay: false,
      display: true,
      editDisabled: false
    },
    {
      type: 'input',
      label: '小麦亩产',
      prop: 'wheatYieldPerMu',
      addDisplay: false,
      display: true,
      editDisabled: false
    },
    {
      type: 'input',
      label: '秋粮亩产',
      prop: 'autumnGrainYieldErMu',
      addDisplay: false,
      display: true,
      editDisabled: false
    },
    {
      type: 'input',
      label: '秋粮总产',
      prop: 'totalAutumnGrainYield',
      addDisplay: false,
      display: true,
      editDisabled: false
    },
    {
      type: 'input',
      label: '水稻亩产',
      prop: 'riceYieldPerMu',
      addDisplay: false,
      display: true,
      editDisabled: false
    },
    {
      type: 'input',
      label: '水稻总产',
      prop: 'totalRiceYield',
      addDisplay: false,
      display: true,
      editDisabled: false
    },
    {
      type: 'input',
      label: '创建人',
      prop: 'createBy',
      addDisplay: false,
      editDisabled: false
    },
    {
      type: 'datetime',
      label: '创建时间',
      prop: 'createTime',
      addDisplay: false,
      editDisabled: false,
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'yyyy-MM-dd HH:mm:ss'
    },
    {
      type: 'datetime',
      label: '更新时间',
      prop: 'updateTime',
      addDisplay: false,
      editDisabled: false,
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'yyyy-MM-dd HH:mm:ss'
    },
    {
      type: 'input',
      label: '更新人',
      prop: 'updateBy',
      addDisplay: false,
      editDisabled: false
    }
  ],
  labelPosition: 'left',
  labelSuffix: '：',
  labelWidth: 120,
  gutter: 0,
  menuBtn: true,
  submitBtn: true,
  submitText: '提交',
  emptyBtn: true,
  emptyText: '清空',
  menuPosition: 'center',
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  searchMenuSpan: 6
}
