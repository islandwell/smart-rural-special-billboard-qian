export const tableOption = {
  column: [{
      type: 'input',
      label: 'id',
      prop: 'id',
      addDisplay: false,
      editDisabled: false,
      span: 11
    },
    {
      type: 'input',
      label: '名称',
      search: true,
      prop: 'name',
      addDisplay: false,
      editDisabled: false,
      display: true
    },
    {
      type: 'input',
      label: '地址',
      prop: 'address',
      addDisplay: false,
      editDisabled: false,
      display: true
    },
    {
      type: 'input',
      label: '经度',
      prop: 'longitude',
      addDisplay: false,
      editDisabled: false,
      display: true
    },
    {
      type: 'input',
      label: '纬度',
      prop: 'latitude',
      addDisplay: false,
      editDisabled: false,
      display: true
    },
    {
      type: 'input',
      label: '坐标系',
      prop: 'pointSystem',
      addDisplay: false,
      editDisabled: false,
      display: true
    },
    {
      type: 'input',
      label: '党支部介绍',
      prop: 'context',
      addDisplay: false,
      editDisabled: false,
      display: true
    },
    {
      type: 'datetime',
      label: '成立时间',
      prop: 'establishmentTime',
      addDisplay: false,
      editDisabled: false,
      display: true,
      format: 'yyyy-MM-dd HH:mm:ss',
      valueFormat: 'yyyy-MM-dd HH:mm:ss'
    },
    {
      type: 'switch',
      label: '状态',
      prop: 'status',
      addDisplay: false,
      editDisabled: false,
      display: true,
      value: 1,
      dicData: [{
          label: '失效',
          value: 0
        },
        {
          label: '生效',
          value: 1
        }
      ],
      detail: false
    },
    {
      type: 'input',
      label: '创建时间',
      prop: 'createTime',
      addDisplay: false,
      editDisabled: true,
      readonly: true
    },
    {
      type: 'input',
      label: '创建人',
      prop: 'createBy',
      addDisplay: false,
      editDisabled: true,
      readonly: true
    },
    {
      type: 'input',
      label: '更新时间',
      prop: 'updateTime',
      addDisplay: false,
      editDisabled: true,
      readonly: true
    },
    {
      type: 'input',
      label: '更新人',
      prop: 'updateBy',
      addDisplay: false,
      editDisabled: true,
      readonly: true
    }
  ],
  labelPosition: 'left',
  labelWidth: 120,
  gutter: 0,
  menuBtn: true,
  submitBtn: true,
  submitText: '提交',
  emptyBtn: true,
  emptyText: '清空',
  menuPosition: 'center',
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  searchMenuSpan: 6
}
