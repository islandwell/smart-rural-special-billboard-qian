export const tableOption = {
  "column": [{
    "type": "input",
    "label": "id",
    "prop": "id",
    "addDisplay": false,
    "editDisabled": false,
    "display": false,
    "readonly": true,
    "detail": true
  }, {
    "type": "input",
    "label": "名称",
    "prop": "name",
    search:true,
    "addDisplay": false,
    "editDisabled": false,
    "display": true
  }, {
    "type": "input",
    "label": "ad编码",
    "prop": "adcode",
    "addDisplay": false,
    "editDisabled": false,
    "display": true
  }, {
    "type": "input",
    "label": "城市编码",
    "prop": "citycode",
    "addDisplay": false,
    "editDisabled": false,
    "display": true
  }, {
    "type": "input",
    "label": "创建时间",
    "prop": "createTime",
    "addDisplay": false,
    "editDisabled": false,
    "readonly": true
  }, {
    "type": "input",
    "label": "创建人",
    "prop": "createBy",
    "addDisplay": false,
    "editDisabled": false,
    "readonly": true
  }, {
    "type": "input",
    "label": "更新时间",
    "prop": "updateTime",
    "addDisplay": false,
    "editDisabled": false,
    "readonly": true
  }, {
    "type": "input",
    "label": "更新人",
    "prop": "updateBy",
    "addDisplay": false,
    "editDisabled": false,
    "readonly": true
  }],
  "labelPosition": "left",
  "labelSuffix": "：",
  "labelWidth": 120,
  "gutter": 0,
  "menuBtn": true,
  "submitBtn": true,
  "submitText": "提交",
  "emptyBtn": true,
  "emptyText": "清空",
  "menuPosition": "center",
  "border": true,
  "index": true,
  "indexLabel": "序号",
  "stripe": true,
  "menuAlign": "center",
  "align": "center",
  "searchMenuSpan": 6,
  "readonly": false
}
