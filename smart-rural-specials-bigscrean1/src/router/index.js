import Vue from 'vue'
import Router from 'vue-router'
import MainPage from '@/views/MainPage'
import {
  auth
} from '@/api/smartpartybuilding'
import notFind from '@/views/404'
import notRole from '@/views/502'

Vue.use(Router)

const router = new Router({
  routes: [{
      path: '/',
      name: 'MainPage',
      component: MainPage
    },
    {
      path: '/404',
      name: 'notFind',
      component: notFind
    },
    {
      path: '/502',
      name: 'notRole',
      component: notRole
    },
    {
      path: "*",
      redirect: "/404"
    }
  ]
})

router.beforeEach(function (to, from, next) {
  let token = ""
  // 获取密码
  if (to.path === '/404' || to.path === '/502') {
    next()
    return
  }
  try {
    let str = window.location.hash.replace("#/?", "")
    if (str.search("secretKey") != -1) {
      let kvs = str.split("&")
      for (let item in kvs) {
        if (kvs[item].split("=")[0] === 'secretKey') {
          token = kvs[item].split("=")[1]
          break
        }
      }
    }
  } catch (err) {}

  let data = {
    token: token,
    url: window.location.href,
    name: '金村数字乡村看板'
  }

  // 外链鉴权 鉴权通过
  auth(data).then(res => {
    console.log(res)
    next() 
  }).catch(err => {
    next({
      path: '/502'
    })
    return
  })

})


export default router;
