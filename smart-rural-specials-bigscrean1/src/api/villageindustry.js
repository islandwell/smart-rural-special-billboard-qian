import request from '@/utils/request'

/**获取基础信息 */
export function getBaseInfo() {
  return request({
    url: `/bulletin/srsbbulletinhousehold/getBaseInfo`,
    method: 'post',
  })
}

/**看板获取商户信息 */
export function getMerchantInfo() {
  return request({
    url: `/bulletin/srsbbulletinmerchantinformation/getMerchantInfo`,
    method: 'post',
  })
}
/**经营种类柱形图 */
export function getMerchantOperation() {
  return request({
    url: `/bulletin/srsbbulletinmerchantinformation/getMerchantOperation`,
    method: 'post',
  })
}
/**获取农作物状态值 */
export function getKanbanCropList() {
  return request({
    url: `/bulletin/srsbbulletincrop/getKanbanCropList`,
    method: 'post',
  })
}

/**获取产业新闻 */
export function getNews(num) {
  return request({
    url: `/bulletin/srsbbulletinindustrynews/getNews/${num}`,
    method: 'post',
  })
}