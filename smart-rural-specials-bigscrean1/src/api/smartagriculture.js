import request from '@/utils/request'

/**查询党建信息 */
export function findSrsbBulletinVillageInformation(name) {
    return request({
        url: `/bulletin/srsbbulletinvillageinformation/findSrsbBulletinVillageInformation/${name}`,
        method: 'post',
    })
}

/**获取智慧农村词云数据 */
export function getWordCloudList() {
    return request({
        url: `/bulletin/srsbbulletinwordcloudinfo/getWordCloudList`,
        method: 'post',
    })
}

/**获取农业产值信息 */
export function getBulletinAgricultural(year) {
    return request({
        url: `/bulletin/srsbbulletinagricultural/getBulletinAgricultural/${year}`,
        method: 'post',
    })
}

/**获取粮食产值数据 */
export function getLiangShiData(year) {
    return request({
        url: `/bulletin/srsbbulletinannualgrainoutput/getLiangShiData/${year}`,
        method: 'post',
    })
}


/**智慧农村新闻 */
export function getNews(num) {
    return request({
        url: `/bulletin/srsbbulletinsmartcountrysidenews/getNews/${num}`,
        method: 'post',
    })
}