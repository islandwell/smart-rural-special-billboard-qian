import request from '@/utils/request'

/**查询党建信息 */
export function getPartyBasicInformation(data) {
    return request({
        url: `/bulletin/smartpartybuildingcontroller/queryPartyBasicInformation/${data}`,
        method: 'get',
    })
}
/**查询天气信息 */
export function getWeatherInformation(){
    return request({
        url: `/bulletin/srsbwether/weatherInformation`,
        method: 'post'
    })
}

/**党支部统计 */
export function queryPartyBranchStatistics(){
    return request({
        url: `/bulletin/srsbbulletinpartymember/queryPartyBranchStatistics`,
        method: 'post'
    })
}
/**年份人数统计 */
export function queryAnnualPopulationStatistics(){
    return request({
        url: `/bulletin/srsbbulletinpartymember/annualPopulationStatistics`,
        method: 'post'
    })
}
/**党员学历情况统计 */
export function queryPartyEducationalStatistics(){
    return request({
        url: `/bulletin/srsbbulletinpartymember/partyEducationalStatistics`,
        method: 'post'
    })
}
/**男女比例 */
export function querySexRatio(){
    return request({
        url: `/bulletin/srsbbulletinpartymember/querySexRatio`,
        method: 'post'
    })
}

/**党员发展 */
export function queryPartyMemberDevelopmentResponse(){
    return request({
        url: `/bulletin/srsbbulletinpartymember/queryPartyMemberDevelopmentResponse`,
        method: 'post'
    })
}

/**外链鉴权 */
export function auth(data){
    return request({
        url: `/bulletin/srsbbulletinlinklist/auth`,
        method: 'post',
        data
    })
}

/**查询所有党支部的数据 */
export function getAllPartyBranchList(){
    return request({
        url: `/bulletin/srsbbulletinpartybranchinfo/getAllPartyBranchList`,
        method: 'post',
    })
}