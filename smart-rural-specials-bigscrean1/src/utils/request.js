//src/utils/request.js
import axios from 'axios'

// 创建 axios 实例
const service = axios.create({
    baseURL: '/api', // url = base url + request url
    timeout: 100000, // request timeout
})

// 请求拦截
axios.interceptors.request.use(
    (config) => {
        console.log("请求拦截->", config)
        return config
    },
    (error) => {
        return error
    }
)

// 响应拦截器
service.interceptors.response.use(
    (response) => {
        let res = response.data
        if (res && res.code == 0) {
            return res
        }
        return Promise.reject(res)


    },
    (error) => {
        //响应错误
        console.log(`错误数据->{}`, error)
        return Promise.reject(error)
    }
)

export default service